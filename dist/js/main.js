$(function () {
  'use strict';

  var doc = $(document);
  var dropdownElement = $('.dropdown');
  var inputElement = dropdownElement.find('input[type="number"]');
  var chooseValueElement = $('input + [data-target-between] li');

  /**
   * Закрываем меню при клике вне него
   */
  var outsideHandler = function (e) {
    if (!dropdownElement.is(e.target) && dropdownElement.has(e.target).length === 0) {
      dropdownElement.removeClass('open');
      doc.off('click', outsideHandler);
    }
  };

  /**
   * Меняем класс `open` и включаем / выключаем слушателя `outsideHandler`
   */
  var toggleMenu = function (e) {
    e.stopPropagation();

    var self = $(this).parent();
    self.toggleClass(function () {
      if ($(this).hasClass('open')) {
        doc.off('click', outsideHandler);
      } else {
        doc.on('click', outsideHandler);

        setTimeout(function () {
          self.find('input').eq(0).focus();
        }, 100);
      }

      return 'open';
    });
  };

  /**
   * Рисуем лейбл
   */
  var renderLabel = function (e) {
    var min = dropdownElement.find('input[name="min"]').val();
    var max = dropdownElement.find('input[name="max"]').val();
    var label = 'Цена';

    if (min) {
      label += ' ' + format("# ##0.####", min);
    }

    if (max) {
      label += ' - ' + format("# ##0.####", max);
    }

    if (!min && !max) {
      label += ' от - до';
    }

    label += ' рублей';

    dropdownElement.find('span').text(label);

    if (typeof e !== 'undefined' && (e.which == 13 || e.which == 27)) {
      dropdownElement.removeClass('open');
      doc.off('click', outsideHandler);
    }
  };

  /**
   * Выбор значения из списка
   */
  var chooseValue = function () {
    var self = $(this);
    var target = self.parent().attr('data-target-between');

    $('input[name="' + target + '"]').val(self.attr('data-target-value'));

    if (target == 'min') {
      dropdownElement.find('input[name="max"]').focus();
    }

    if (target == 'max') {
      dropdownElement.removeClass('open');
      doc.off('click', outsideHandler);
    }

    return renderLabel();
  };

  dropdownElement.find('.dropdown__button').on('click', toggleMenu);
  chooseValueElement.on('click', chooseValue);
  inputElement.on('keyup', renderLabel);
});